<?php
include_once("tp3-helpers.php");

// Fonction permettant d'afficher le fichier Json récupérer a une url gràace à curl
function AfficheJSonCurl($url) {
    print_r(smartcurl($url));
}

function Contenu($ID){
    $contenu_brut_va=tmdbget($ID,null);
    $contenu_json_va=json_decode($contenu_brut_va,true);

    $contenu_brut_vo=tmdbget($ID,array("language" => $contenu_json_va["original_language"]));
    $contenu_json_vo=json_decode($contenu_brut_vo,true);
    
    $contenu_brut_vf=tmdbget($ID,array("language" => "fr"));
    $contenu_json_vf=json_decode($contenu_brut_vf,true);
    
    //Version originale
    $contenu['Titre original']['vo'] = $contenu_json_vo['original_title'];
    $contenu['Titre']['vo'] = $contenu_json_vo['title'];
    $contenu['Tag(s)']['vo'] = $contenu_json_vo['tagline'];
    $contenu['Résumé']['vo'] = $contenu_json_vo['overview'];
    $contenu ['affiche']['vo']=$contenu_json_vo['poster_path'];
    $contenu['Lien']['vo'] = $contenu_json_vo['homepage'];


    //Version anglaise
    $contenu['Titre original']['va'] = $contenu_json_va['original_title'];
    $contenu['Titre']['va'] = $contenu_json_va['title'];
    $contenu['Tag(s)']['va'] = $contenu_json_va['tagline'];
    $contenu['Résumé']['va'] = $contenu_json_va['overview'];
    $contenu ['affiche']['va']=$contenu_json_va['poster_path'];
    $contenu['Lien']['va'] = $contenu_json_va['homepage'];


    //Version Francaise
    $contenu['Titre original']['vf'] = $contenu_json_vf['original_title'];
    $contenu['Titre']['vf'] = $contenu_json_vf['title'];
    $contenu['Tag(s)']['vf'] = $contenu_json_vf['tagline'];
    $contenu['Résumé']['vf'] = $contenu_json_vf['overview'];
    $contenu ['affiche']['vf']=$contenu_json_vf['poster_path'];
    $contenu['Lien']['vf'] = $contenu_json_vf['homepage'];


    return $contenu;
}

function affichage($contenu,$ID){
    echo "<table>";
    echo "<thead> <tr> <td> </td>";
    echo "<td> Version Originale </td>";
    echo "<td> Version Anglaise </td>";
    echo "<td> Version Française </td> </tr> </thead>";
    foreach ($contenu as $key => $value){
        
        if ($key != 'Lien' && $key != 'id' && $key !='affiche'){
            echo "<tr>";
            echo "<td>" . $key . "</td> ";
            echo "<td>" . $value["vo"] . "</td>";
            echo "<td>" . $value["va"] . "</td>";
            echo "<td>" . $value["vf"] . "</td>";
            echo "</tr>";
        }
        
        elseif ($key =='affiche'){
            echo "<tr>";
            echo "<td>" . $key . "</td> ";
            echo "<td>" ."<img src=\"https://image.tmdb.org/t/p/w185" . $value['vo'] . "\" alt= \"Image du film\" >". "</td>";  
            echo "<td>" ."<img src=\"https://image.tmdb.org/t/p/w185" . $value['va'] . "\" alt= \"Image du film\" >". "</td>";  
            echo "<td>" ."<img src=\"https://image.tmdb.org/t/p/w185" . $value['vf'] . "\" alt= \"Image du film\" >". "</td>";  
            echo "</tr>";
        }

        elseif ($key =='Lien'){
            echo "<tr>";
            echo "<td colspan=\"4\">";
            echo "<a href=\"https://www.themoviedb.org/movie/" . $ID . "\">Lien vers le film  </a> <br \> ";
            echo "</td>";
            echo "</tr>";
        }
    }
    echo "<table>";

    //Affichage du trailer:
    echo "<td colspan=\"4\">";
    echo "<iframe width=\"540\" height=\"360\"src=\"".get_trailer($ID)."\"></iframe>";

}



?>


<html>
    <head>
        <title>Film selon son ID sur TMDB</title>
    </head>
    <body>
        <?php
        echo " <Strong>Voici les informations du film ";
        affichage(Contenu(634649),634649);
        q7(634649);
        ?>
    </body>
</html>

