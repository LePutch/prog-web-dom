% PW-DOM  Compte rendu de TP 3

# Compte-rendu de TP 3

Sujet choisi : TMDB

## Participants 

* BRACQUIER Benjamin
* ISABELLE William
* PUCCI Jeremy


1. **Exploration**. Quel est le format de réponse ? De quel film s'agit-il ? Essayez avec le paramètre supplémentaire `language=fr`.

Le format du ficher obtenu est .json. 
Il s'agit du film "Fight club".
En ajoutant &language=fr à la fin de l'url, on obtient le json avec tous les textes en français.


2. **Exploration CLI**. Testez également le service avec `curl` en ligne de commande, puis avec un programme php minimal utilisant `tp3-helper.php`.

En utilisant curl, on récupère le fichier json dans le terminal. Puis on l'affiche grâce à la fonction print_r()


3,4,5,10. **Page de détail (web)**. 

Pour les questions 3,4,5 et 10,les codes sont dans le fichier program.php .
Ce fichier permet de renseigner pour un film:son titre, son titre original, sa tagline (si elle existe), sa description, un lien vers la page publique TMDB ,son poster en version originale, version anglaise, et version française ainsi que sa bande annonce.

6.Le fichier q6.php est une page web dynamique à ouvrir dans le navigateur.

Le code PHP va rechercher tous les films de la collection du Seigneur des Anneaux et va les afficher dans la page web générée.

7,9.Le code pour les questions 7 et 9 sont dans le fichier q7-9.php .
Pour répondre à la 9, nous avons repris le code de la question 7 en ajoutant un lien au nom chaque acteur, renvoyant vers leur page TMDB respective.

En effet, cette page renseigne tous les films joués par chaque acteur, ainsi que leur rôle dans chacun des films.
Le fichier q7-9.php est une page web dynamique à ouvrir dans le navigateur.

Le code PHP va rechercher et traiter les données des acteurs de la collection du Seigneur des Anneaux et va les afficher dans la page web générée.

8.Nous ne savons pas s'il est possible d'afficher uniquement les acteurs jouant des hobbits.


Mais nous avons penser à 1 solutions :
    
    - On aurait pû afficher seulement les acteurs dont le rôle contient le mot Hobbit , mais pour le rôle de Frodo Baggins, ce mot-clé n'est pas présent.
    
   