<?php

/**
 * TMDB query function
 * @param string $urlcomponent (after the prefix)
 * @param array (associative) GET parameters (ex. ['language' => 'fr'])
 * @return string $content
**/
function tmdbget($urlcomponent, $params=null) {
    $apikey = 'ebb02613ce5a2ae58fde00f4db95a9c1';
    $apiprefix = 'http://api.themoviedb.org/3/movie/';  //3rd API version
	
	$targeturl = $apiprefix . $urlcomponent . '?api_key=' . $apikey;
    $targeturl .= (isset($params) ? '&' . http_build_query($params) : '');
    list($content, $info) = smartcurl($targeturl);

    return $content;
}


function tmdbget_videos($urlcomponent, $params = null)
{
    $apikey = 'ebb02613ce5a2ae58fde00f4db95a9c1';
    $apiprefix = 'http://api.themoviedb.org/3/';  //3rd API version

    $targeturl = $apiprefix . $urlcomponent . '?api_key=' . $apikey."&append_to_response=videos";
    $targeturl .= (isset($params) ? '&' . http_build_query($params) : '');
    list($content, $info) = smartcurl($targeturl);

    return $content;
}

/**
 * @param int $ID
 * @return string $url_youtube
 **/  
function get_trailer($ID){
    $url_to_send = "movie/".$ID."/videos"; 
    $contenu = tmdbget_videos($url_to_send);
    $contenu_json = json_decode($contenu, true);
    $key_youtube = $contenu_json["results"][0]["key"];
    $url_youtube = "https://www.youtube.com/embed/".$key_youtube;
    return $url_youtube;
    }

/**
 * curl wrapper
 * @param string $url
 * @return string $content
 **/  
function smartcurl($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, "php-libcurl");
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

    $rawcontent = curl_exec($ch);
    $info = curl_getinfo($ch);
    curl_close($ch);
    return [$rawcontent, $info];
}
