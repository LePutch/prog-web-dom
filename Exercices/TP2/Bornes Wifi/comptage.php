<?php

//Q3
//on lit le fichier .csv et on met le resultat dans un tableau
$array = file("bornes2wifi.csv");

//on compte le nombre de ligne 
$nombrePointsAcces = count($array) - 1;
echo "Le nombre total de points d'accés est : " . $nombrePointsAcces ;
print_r("<br>");

//Q4
require_once('../../../Helpers/tp2-helpers.php');


function structure(){
    $row=0;
    if (($ouvrir = fopen("bornes2wifi.csv", "r")) !== FALSE) {
        //bornes2wifi est le fichier bien encodé avec les bons caractères
        while (($data = fgetcsv($ouvrir, 0, ",")) !== FALSE) {
            $pointWifi[$row] = initAccesspoint($data);
            print_r($pointWifi[$row]);
            print_r("<br>");
            $row++;
        }
        fclose($ouvrir);
        return $pointWifi;
    }
}

$pointWifi=structure();

//Q5
//on mets un point fixe à une certaine coordonée
$PlaceGrenette = array("lon" => 5.72752, "lat" => 45.19102);

$i=0;
//Affichage de tous les points wifi avec leur distance
for ($i = 0; $i < count($pointWifi); $i++) {
    $distance[$i] = distance($PlaceGrenette, $pointWifi[$i]);
    echo "Distance : " . $distance[$i] . "m  -  Borne Wifi : " . $pointWifi[$i]["name"] . " - " . $pointWifi[$i]["adr"];
    print_r("<br>");

}


//Tous les points d'accés à moins de 200 m

$Wifi200m = 0; //Compteur du nombre de points d'accès à moins de 200m
$Pluspres=200; // Variable pour connaitre le point d'accès le plus proche
$NomPlusProche;
print_r("<br>");
echo "Voici les bornes WIFI à moins de 200m de la place Grenette";
print_r("<br>");

for ($i = 0; $i < count($pointWifi); $i++){
    if($distance[$i]<200){
    echo "Distance : " . $distance[$i] . "m  -  Borne Wifi : " . $pointWifi[$i]["name"] . " - " . $pointWifi[$i]["adr"];
    print_r("<br>");
    $Wifi200m++;
    if($distance[$i]<$Pluspres){
        $Pluspres=$distance[$i];
        $NomPlusProche=$pointWifi[$i]["name"];
    }
    }
}

print_r("<br>");
echo "Le nombre de points d'accés à moins de 200 m est de: " . $Wifi200m;
print_r("<br>");
echo "Le point d'accès WIFI le plus proche de la place Grenette est: " . $NomPlusProche;
print_r("<br><br>");


//Q6
//Initialisation du paramètre N
$N=10;

//Point gps de la place grenette
$Reference = array( 
    'lon' => 5.72752,
    'lat' => 45.19102
);

//On crée un tableau des distances
foreach($pointWifi as $point) {
    $DIST[] = distance($Reference, $point);
}

// On trie par ordre croissant le tableau des distances
array_multisort($DIST, SORT_ASC, $pointWifi);

//Les points les plus proches de nous
$point_proche=array_slice($pointWifi,0,$N);
echo "Voici les ".$N." plus proches points d'accès de nous";
print_r("<br>");
foreach($point_proche as $point) {
    echo $point['name'] . " , " . $point['adr'] ;
    print_r("<br>");
}

print_r("<br><br>");


//Q7
echo "Voici l'adresse de chaque point d'accès:";
print_r("<br><br>");
for ($i = 0; $i < count($pointWifi); $i++) {
    // On ajoute les adresses au tableau des points d'accès
    $pointWifi[$i]["address"] = json_decode(smartcurl("https://api-adresse.data.gouv.fr/reverse/?lon=" . $pointWifi[$i]["lon"] . "&lat=" . $pointWifi[$i]["lat"], 0), true)["features"][0]["properties"]["label"];
    // On affiche ces adresses une par une
    $pt = $pointWifi[$i];
    echo "Point:   Lat: " . $pt["lat"] . "   Lon: " . $pt["lon"] . "   Address: " . $pt["address"];
    print_r("<br>");
}


//Q8
$ref_point = array('lon' => (float) $_GET["lon"], 'lat' => (float) $_GET["lat"]);

    foreach($pointWifi as $pointWifi) {
        $distances[] = distance($ref_point, $pointWifi);
    }
    array_multisort($distances, SORT_ASC, $pointWifi);
    $top_points = array_slice($pointWifi, 0, (int)$_GET["top"]);

    for ($i = 0; $i < count($top_points); $i++) {
        $top_points[$i]["address"] = json_decode(smartcurl("https://api-adresse.data.gouv.fr/reverse/?lon=" . $top_points[$i]["lon"] . "&lat=" . $top_points[$i]["lat"], 0), true)["features"][0]["properties"]["label"];
        $top_points[$i]["dst"] = $distances[$i];
    }

    echo json_encode($top_points);
//Q9
$json = json_decode(file_get_contents("Données/borneswifi_EPSG4326_20171004.json"), true);

$pointWifi = [];
foreach ($json["features"] as $json_point) {
    $pointWifi[] = array(
        'name' => $json_point["properties"]["AP_ANTENNE1"],
        'adr' => $json_point["properties"]["Antenne 1"],
        'lon' => $json_point["geometry"]["coordinates"][0],
        'lat' => $json_point["geometry"]["coordinates"][1]
    );
}
    
    //var_dump($pointWifi);









?>
