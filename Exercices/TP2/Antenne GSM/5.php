<?php

require_once("antenne.php");

$antennes = getFileCSV();
$nbAntennes = getNombreAntennes($antennes);
echoNbrAntennes($nbAntennes);

$operateurs = getOperateurs();
echoOperateurs($operateurs);

$antennes = getAntennes();

$ref_point = array('lon' => (float) $_GET["lon"], 'lat' => (float) $_GET["lat"]);

foreach($antennes as $antenne) {
    $point = geopoint($antenne['lon'], $antenne['lat']);
    $distances[] = distance($ref_point, $point);
}

array_multisort($antennes, SORT_ASC, $distances);
$top_points = array_slice($points, 0, (int)$_GET["top"]);

for ($i = 0; $i < count($top_points); $i++) {
    $top_points[$i]["address"] = json_decode(smartcurl("https://api-adresse.data.gouv.fr/reverse/?lon=" . $top_points[$i]["lon"] . "&lat=" . $top_points[$i]["lat"], 0), true)["features"][0]["properties"]["label"];
    $top_points[$i]["dst"] = $distances[$i];
}
    
    

echo '<!DOCTYPE html>
    <html>';
    echo '<head>';
        echo '<title>' . $_GET["top"] . ' antennes trouvées' . '</title>';
        echo '<meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <style type="text/css">
            table {
                border-collapse: collapse;
            }
            td, th {
                border: solid black 1px;
                padding: 1em;
            }
        </style>';
    echo '</head>';
    echo '<body>';
        echo '<h2>Voici les ' . $_GET["top"] . ' points d\'accès les plus proches</h2>';
        echo '<table>
            <tr>
                <th>Nom</th>
                <th>Adresse</th>
                <th>Distance</th>
                <th>Latitude</th>
                <th>Longitude</th>
            </tr>';
            foreach ($top_points as $antenne) {
                echo "<tr>\n";
                echo "  <td>" . $antenne["name"] . "</td>\n";
                echo "  <td>" . $antenne["address"] . "</td>\n";
                echo "  <td>" . $antenne["dst"] . "</td>\n";
                echo "  <td>" . $antenne["lat"] . "</td>\n";
                echo "  <td>" . $antenne["lon"] . "</td>\n";
                echo "</tr>\n";
            }
        echo '</table>
    </body>
</html>';

?>