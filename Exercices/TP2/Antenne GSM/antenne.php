<?php


require_once('../../../Helpers/tp2-helpers.php');

function getFileCSV()
{
	$array = file("antenneGSM2.csv");
	return $array;
}

//Q1
function getNombreAntennes($array)
{
	$nombrePointsAcces = count($array) - 1;
	return $nombrePointsAcces;

}
function echoNbrAntennes($nbAnt)
{
	echo "Nombre total de points d'accés : " . $nbAnt;
	print_r("<br>");
}

//Q2
function getOperateurs()
{
	$strfile = "antenne.json";
	$json = json_decode(file_get_contents($strfile), true);
	$operateurs = [];
	foreach ($json["features"] as $antenne) {
		$ope = $antenne["properties"]["OPERATEUR"];
		if (!array_key_exists($ope, $operateurs)) {
			$operateurs[$ope] = 0;
		}
		$operateurs[$ope]++;
	}
	return $operateurs;
}

function echoOperateurs($operateurs)
{
	echo "Ce jeu de données décrit les antennes de " . count($operateurs) . " opérateurs différents";
	print_r("<br>");
	foreach($operateurs as $op => $count) {
		echo "$op possède $count antennes";
		print_r("<br>");
	}
}
$json = json_decode(file_get_contents("Données/borneswifi_EPSG4326_20171004.json"), true);

$pointWifi = [];
foreach ($json["features"] as $json_point) {
    $pointWifi[] = array(
        'name' => $json_point["properties"]["AP_ANTENNE1"],
        'adr' => $json_point["properties"]["Antenne 1"],
        'lon' => $json_point["geometry"]["coordinates"][0],
        'lat' => $json_point["geometry"]["coordinates"][1]
    );
}

function getAntennes()
{
	$json = json_decode(file_get_contents("antenne.json"), true);
	$antennes= [];
	foreach ($json["Feature"] as $json_point) {
		$antennes[] = array(
			'name' => "Nom Temp",//$json_point["properties"]["AP_ANTENNE1"],
			'adr' => $json_point["properties"]["ANT_ADRES_LIBEL"],
			'lon' => $json_point["coordinates"][0],
			'lat' => $json_point["coordinates"][1]
		);
	}
	return $antennes;
}

?>
