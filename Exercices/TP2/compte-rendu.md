% PW-DOM  Compte rendu de TP2

# Compte-rendu de TP

Sujet choisi : TP2

## Participants 

* PUCCI Jérémy
* BRACQUIER Benjamin
* ISABELLE William

## Bornes Wifi 

0. **Visualisation texte**.

Nous avons encodé au bon format et enregistré le fichier sous le nom de bornes2wifi.csv grâce à la commande iconv -f latin1 -t utf8 borneswifi.csv > bornes2wifi.csv.

1. **Comptage**. 

La commande suivante permet d'évaluer le nombre de points d'accès : 
wc -l borneswifi.csv

On trouve 69 lignes dont 68 qui sont des points d'accés (la première ligne est le titre).
 

2. **Points multiples**

cut -d, -f2 antennes.csv|sort|uniq|wc-l

cut -d, signifie qu'on va découper le fichier grâce au délimiteur ','; -f2 signifie que l'on garde seulement le deuxième champ, on a ensuite le nom du fichier qu'on traite. 'sort' signifie que l'on trie par ordre alphabétique les données. 'uniq' sert à garder seulement une occurence des lignes redondantes. Enfin wc -l sert à compter le nombre de données. On trouve 59 emplacements différents.

Pour trouver le lieu qui a le plus de points d'accès, on utilise la commande:

cut -d, -f2 antennes.csv|sort|uniq-c|sort-n -r -b| head -1

On trouve que c'est la bibliothéque Etudes qui en a le plus avec 5 points d'accés.

3. **Comptage PHP**

On retrouve le code dans le fichier 'comptage.php' mais le voici quand même:

//on lit le fichier .csv et on met le resultat dans un tableau
$array = file("bornes2wifi.csv");

//on compte le nombre de ligne 
$nombrePointsAcces = count($array) - 1;
echo "Le nombre total de points d'accés est : " . $nombrePointsAcces ;
print_r("<br>");

4. **Structure de données**. 

Nous avons utilisé la fonction initAccesspoint donnée dans le fichier tp2-helpers.php qui nous renvoie un tableau associatif de clés.

Notre fonction se nomme 'structure' et se trouve dans comptage.php.

5. **Proximité**. 

De même, le code est lisible dans le source comptage.php.
Nous obtenons qu'il y a 7 points d'accés à moins de 200 m de la place Grenette, les voici:
Distance : 112.8m - Borne Wifi : AP_ANTENNE2 - Antenne 2
Distance : 73.7m - Borne Wifi : AP_APP_GAGNANT - Musée Stendhal Appartement Gagnon
Distance : 145.9m - Borne Wifi : AP_JDV1 - Jardin de ville AP switch 470Bis
Distance : 128.9m - Borne Wifi : AP_JDV2 - Jardin de ville Port2
Distance : 181.4m - Borne Wifi : AP_THEATRE1 - Théatre
Distance : 181.4m - Borne Wifi : AP_THEATRE2 - Théatre
Distance : 20.1m - Borne Wifi : Place Grenette - 8 place Grenette, 1er étage (neptune)
 
Le point d'accès WIFI le plus proche de la place Grenette est: Place Grenette

6. **Proximité top N**. 

De même, code consultable dans la même source. Nous avons mis une variable globale N qui peut être modifiée à la main pour trouver les N points d'accès les plus proches de nous.

7. **Géocodage**. 
Code consultable dans la même source et pour avoir ces adresses,nous avons utlisé le géocodeur officiel français avec un API et la fonction smartcurl().

8. **Webservice**. 
Nous avons créer une page html pour ce webservice qui s'appelle 8.html et le code conçernant cette page est toujours dans la même source où nous avons utilisé les fonctions array_multisort() et array_slice() pour un rendu qui est du plus proche au plus loin.

9. **Format json**. 
Le code peut être regardé sur la même source et nous avons bien utilisé la fonction json_decode.

10. **Client webservice**. 
Pour faire ce client webservice,nous avons créer 1 page php et 1 page html qui s'appellent 10.php et 10.html.

## Antennes GSM

Notre code se trouve dans le source 'antenne.php'. De plus, nous avons encodé au bon format et enregistré le fichier sous le nom de antenneGSM2.csv grâce à la commande iconv -f latin1 -t utf8 antenneGSM.csv > antenneGSM2.csv.
 

1. **CSV Antennes**. 

On trouve grâce à la commande $nombrePointsAcces = count($array) - 1; qu'il y a 100 points d'accès. Le -1 sert à enlever la première ligne qui constitue les légendes.

Les informations supplémentaires sont l'ID de l'antenne, son adresse, l'opérateur qui l'exploite, l'utilisation ou non de la technologie microcell, la technologie de l'antenne (2G/3G/4G), son numéro cartoradio et son numéro de support. Ces informations sont intéressantes car elles permettent de totalement décrire le point d'accès.

2. **Statistiques opérateurs**. 

On remarque que 4 opérateurs sont présents dans le jeu de données. Deux boucles foreach permettent de calculer facilement le nombre d'antennes que chacun possède.

On trouve que BYG possède 26 antennes, SFR possède 30 antennes, ORA possède 26 antennes et FREE possède 18 antennes.

3. **KML validation**. 

La validation syntaxique du document est possible par exemple grâce à xmllint, néanmoins la validation schématique est impossible car aucun schéma n'est fourni avec le document.

4. **KML bis**. 

Le fichier d'origine est lourd et beaucoup d'informations se répetent. La lecture du fichier brut est difficile pour nous. 

5. **Top N opérateur**. 
